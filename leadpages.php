<?php

/*
Plugin Name: Leadpages Connector
Plugin URI: http://leadpages.net
Description: A brief description of the Plugin.
Version: 2.0
Author: Leadpages
Author URI: http://leadpages.net
License: GPL2
*/


/*
  |--------------------------------------------------------------------------
  | Application Entry Point
  |--------------------------------------------------------------------------
  |
  | This will be your plugin entry point. This file should
  | not contain any logic for your plugin what so ever.
  |
  */

include 'App\Bootstrap\FrontBoostrap.php';
include 'App\Bootstrap\AdminBootstrap.php';